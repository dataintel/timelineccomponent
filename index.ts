import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BasewidgetTimelineComponent } from './src/basewidget.component';
import { MaterialModule } from '@angular/material';


export * from './src/basewidget.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule

  ],
  declarations: [
    BasewidgetTimelineComponent
  ],
  exports: [
    BasewidgetTimelineComponent
  ]
})
export class TimeLineModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TimeLineModule,
      providers: []
    };
  }
}
