import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timeline',
  template : `<style>
  /* Let's get this party started */
::-webkit-scrollbar {
    width: 12px;
}
 
/* Track */
::-webkit-scrollbar-track {
    /*-webkit-box-shadow: inset 0px 0 2px #c7c7c7; 
    */
    background: rgb(248, 248, 248);
    -webkit-border-radius: 0px;
    border-radius: 0px;
}
 
/* Handle */
::-webkit-scrollbar-thumb {
    -webkit-border-radius: 5px;
    border-radius: 5px;
    background: slategrey; 
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5); 
}
::-webkit-scrollbar-thumb:window-inactive {
    background: gray; 
}

md-list-item{
    margin: 2px 2px 2px 2px;
    margin-left: -13px;
    margin-right: -13px;
}
</style>

<div class="col-12 " style="padding: 10px">
<div id="utama" class="col-md-4" style="max-height:300px;overflow-y:scroll;overflow-x:hidden;">
  <md-list>
    <div  *ngFor ="let datacarrier of dataLoader">
      <md-list-item >
      

        <p md-line>
          <span>{{datacarrier.title}}</span>
        </p>
        <h3 md-line><small style="font-size:12px;">{{datacarrier.media_displayName}} . {{datacarrier.timestamp}} </small></h3>
      </md-list-item>
       <md-divider></md-divider>
    </div>
   
  </md-list>
</div>
<div id="utama" class="col-md-4" style="max-height:300px;overflow-y:scroll;overflow-x:hidden;">
  <md-list>
    <div  *ngFor ="let datacarrier1 of dataLoader1">
      <md-list-item >
      

        <p md-line>
          <span>{{datacarrier1.title}}</span>
        </p>
        <h3 md-line><small style="font-size:12px;">{{datacarrier1.media_displayName}} . {{datacarrier1.timestamp}} </small></h3>
      </md-list-item>
       <md-divider></md-divider>
    </div>
   
  </md-list>
</div>
<div id="utama" class="col-md-4" style="max-height:300px;overflow-y:scroll;overflow-x:hidden;">
  <md-list>
    <div  *ngFor ="let datacarrier2 of dataLoader2">
      <md-list-item >
      

        <p md-line>
          <span>{{datacarrier2.title}}</span>
        </p>
        <h3 md-line><small style="font-size:12px;">{{datacarrier2.media_displayName}} . {{datacarrier2.timestamp}} </small></h3>
      </md-list-item>
       <md-divider></md-divider>
    </div>
   
  </md-list>
</div>
</div>` 
})
export class BasewidgetTimelineComponent implements OnInit {

  public dataLoader: Array<any> = [
	 {
      "title" : "British Museum show charts America pop art",
      "timestamp" : "17 days ago",
      "media_displayName":"The Malay Mail Online"
    },
    {
      "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"The Malaysian Times"
  },
    {
      "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
     },
    {
       "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
    },
    {
     "title" : "Exhibition charting American pop art opens at London's British Museum",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
     }];

     public dataLoader1: Array<any> = [
	 {
      "title" : "geely saya it doubles earnings during 2016",
      "timestamp" : "17 days ago",
      "media_displayName":"The Malay Mail Online"
    },
    {
      "title" : "China Offers Attractive Finacial Pckage For ECRL Project",
      "timestamp" : "17 days ago",
      "media_displayName":"The Malaysian Times"
  },
    {
      "title" : "Israel hails progress in talks on curbing settlement expansion",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
     },
    {
       "title" : "Chinese Vice Premier confident in China's economy",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
    },
    {
     "title" : "Kiwi stable as investor await us vote",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
     }];
     public dataLoader2: Array<any> = [
	 {
      "title" : "US new home sales hit 7-month high; jobless claims rise",
      "timestamp" : "17 days ago",
      "media_displayName":"The Malay Mail Online"
    },
    {
      "title" : "Nasdaq little changed; Dow draggged down by NIke",
      "timestamp" : "17 days ago",
      "media_displayName":"The Malaysian Times"
  },
    {
      "title" : "Afghan Taliban signal  growing strength with capture of southem district",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
     },
    {
       "title" : "A top Wall Street banker has quit his jobs to fight for civil liberties",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
    },
    {
     "title" : "How a border tax could divide boeing and its suppliers",
      "timestamp" : "17 days ago",
      "media_displayName":"Channel News Asia"
     }];

    

  constructor() {console.log(this.dataLoader);}
  
  ngOnInit() { 
  }

}
